# Генератор конфигураций

> Не трать свою жизнь на копи-паст

![Сгенерируй](https://www.pvsm.ru/images/2016/08/03/generator-konfiguracii-dlya-setevogo-oborudovaniya-i-ne-tolko.png)


## Для чего?

Каждый день настраивая новое оборудование, тебе приходится копировать 
части конфигурации из своих шаблонов и подставлять нужные данные!

*В каждой строке может быть ошибка*

## Используй
Вызови функции по созданию интерфейсов, генерации Access-list's, настроки hsrp и все это без копи-паста из шаблона и ошибок!

Для добавления нового vlan в trunk:
```py
def add_vlan_to_trunk(interface, vlan):
    answer = (
        f'interface {interface}',
        f'switchport trunk allowed vlan add {vlan}',

    )
    return answer
```
Не сбей весь список вланов, а добавь только нужный. Вызови функцию: 
```
add_vlan_to_trunk(interface, vlan)
```

Не понял о чем я? Почитай в [`гайде`](http://xgu.ru/wiki/VLAN_%D0%B2_Cisco).

Что делают другие функции, так же понятно из их названия:

1. generate_acl(name)
2. create_interface(name, access_or_trunk, acl_name)
3. create_hsrp_interface(name, network, acl_name)

## Установи

С помощью [pip](https://pypi.org/project/pip/)

```
$ pip install generate-config
```

## Лицензия

MIT